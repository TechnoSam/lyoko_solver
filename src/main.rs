extern crate clap;
extern crate toml;

use clap::{App, Arg};
use serde::Deserialize;

use lyoko_solver::ansi;
use lyoko_solver::compound_square_shape as css;
use lyoko_solver::compound_square_shape_file_parser::CssfpError as cssfpe;
use lyoko_solver::puzzle;

use std::fs;
use std::io;
use std::io::prelude::*;
use std::process;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

/// Defines how a config item should be parsed into a color
#[derive(Deserialize)]
struct ConfigColor {
    /// The foreground (text) color.
    ///
    /// The string will later be parsed into an ansi::Color.
    fg: Option<String>,
    /// The background color.
    ///
    /// The string will later be parsed into an ansi::Color.
    bg: Option<String>,
}

/// Defines how a config file should be parsed
#[derive(Deserialize)]
struct Config {
    /// List of colors to use for displaying the puzzle.
    colors: Option<Vec<ConfigColor>>,
}

fn main() {
    const VERSION: &'static str = env!("CARGO_PKG_VERSION");

    let matches = App::new("Lyoko Solver")
        .version(VERSION)
        .about("Solves hacking puzzles from Code Lyoko: Get Ready to Virtualize")
        .arg(
            Arg::with_name("PUZZLE_FILE")
                .help("Sets the puzzle file to use")
                .required(true)
                .index(1)
        )
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true)
        )
        .get_matches();

    let puzzle_file = matches.value_of("PUZZLE_FILE").unwrap();
    let config_file_name = matches.value_of("config").unwrap_or("config.toml");

    let config = fs::read_to_string(config_file_name);
    let config = match config {
        Err(_) => {
            eprintln!("Could not read from config file: {}", config_file_name);
            process::exit(1);
        },
        Ok(x) => x,
    };

    let config: Config = match toml::from_str(&config) {
        Err(e) => {
            eprintln!("Failed to parse config file: {}", e);
            process::exit(1);
        },
        Ok(x) => x,
    };

    let mut colors = vec![];
    match &config.colors {
        None => (),
        Some(conf_colors) => {
            for color in conf_colors {
                let fg: ansi::Fg = match &color.fg {
                    None => ansi::Fg::None,
                    Some(x) => {
                        match x.parse() {
                            Err(_) => {
                                eprintln!("Failed to parse color: {}", x);
                                std::process::exit(1);
                            },
                            Ok(y) => y,
                        }
                    },
                };
                let bg: ansi::Bg = match &color.bg {
                    None => ansi::Bg::None,
                    Some(x) => {
                        match x.parse() {
                            Err(_) => {
                                eprintln!("Failed to parse color: {}", x);
                                std::process::exit(1);
                            },
                            Ok(y) => y,
                        }
                    },
                };
                colors.push(ansi::Color::new(fg, bg))
            }
        },
    }

    let chars = vec![
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    ];

    let puzzle = lyoko_solver::file_to_puzzle(puzzle_file);
    let puzzle = match puzzle {
        Err(e) => {
            let message = match e {
                cssfpe::FileNotFound => "File not found",
                cssfpe::FileMalformed => "File could not be parsed correctly",
                cssfpe::DataNonAscii => "Non-ASCII Data found in file",
                cssfpe::PuzzleMalformed => "Puzzle is invalid",
            };
            eprintln!("{}", message);
            process::exit(1);
        },
        Ok(x) => x,
    };

    let check = puzzle::display_unsolved(&puzzle, &chars, &colors);
    match check {
        Err(e) => {
            match e {
                puzzle::PuzzleDisplayError::TooManyPieces => panic!("Too many pieces"),
                puzzle::PuzzleDisplayError::MismatchedSolution => panic!("Mismatched sln"),
            }
        },
        Ok(_) => (),
    }

    // We want to compute the solution in a separate thread so it won't look like the program has
    // frozen while it thinks.

    let (tx, rx) = mpsc::channel();
    let puzzle_clone = puzzle.clone();

    thread::spawn(move || {
        let sln = lyoko_solver::solve(&puzzle_clone.board, &puzzle_clone.pieces);
        let check = tx.send(sln);
        match check {
            Ok(_) => (),
            Err(_) => {
                panic!("Receiver closed");
            }
        }
    });

    // We need to wait for the result and show that we're thinking while we wait

    let sln: Option<Vec<Option<css::Point2D>>>;
    // Brief wait so we can terminate immediately if the solution was quickly found
    thread::sleep(Duration::from_millis(100));
    let mut thinking_printed = false;
    loop {
        let msg = rx.try_recv();
        match msg {
            Ok(x) => {
                sln = x;
                if thinking_printed {
                    println!("\n");
                }
                break;
            },
            Err(mpsc::TryRecvError::Empty) => {
                if thinking_printed {
                    print!(".");
                    io::stdout().flush().unwrap();
                }
                else {
                    thinking_printed = true;
                    print!("Thinking...");
                    io::stdout().flush().unwrap();
                }
            },
            Err(mpsc::TryRecvError::Disconnected) => {
                panic!("Transmitter closed");
            },
        };
        thread::sleep(Duration::from_millis(1000));
    }
    let sln = match sln {
        None => {
            println!("No solutions found");
            return;
        },
        Some(x) => x,
    };

    let check = puzzle::display_solved(&puzzle, &sln, &chars, &colors);
    match check {
        Err(e) => {
            match e {
                puzzle::PuzzleDisplayError::TooManyPieces => panic!("Too many pieces"),
                puzzle::PuzzleDisplayError::MismatchedSolution => panic!("Mismatched sln"),
            }
        },
        Ok(_) => (),
    }
}
