//! Used for managing 2D shapes made up of square blocks.
//!
//! Both the board and the pieces are made up of
//! [CompoundSquareShape](struct.CompoundSquareShape.html)s, this module provides everything needed
//! to create and interact with them.

/// A basic 2D point for representing positions.
///
/// For this application, all points are positive integers, with (0, 0) in the upper left corner.
#[derive(Clone, Debug)]
pub struct Point2D {
    /// The x-coordinate (horizontal)
    pub x: usize,
    /// The y-coordinate (vertical)
    pub y: usize,
}

/// Represents the smallest element of a CompoundSquareShape.
#[derive(Clone, Debug, PartialEq)]
pub enum Cell {
    /// The cell is filled in.
    Present,
    /// The cell is empty.
    Absent,
}

/// Represents a shape made up of square shaped blocks or "cells".
#[derive(Clone, Debug)]
pub struct CompoundSquareShape {
    /// 2D Vector of [Cell](enum.Cell.html)s representing the shape.
    data: Vec<Vec<Cell>>,
    /// Cached width of the shape.
    width: usize,
    /// Cached height of the shape.
    height: usize,
}

/// Errors that can occur when attempting to construct a
/// [CompoundSquareShape](struct.CompoundSquareShape.html).
#[derive(Debug)]
pub enum CompoundSquareShapeError {
    /// The supplied shape data is empty.
    Empty,
    /// The supplied shape data has mismatched row lengths.
    NotSquare,
}

impl CompoundSquareShape {
    /// Constructs a new [CompoundSquareShape](struct.CompoundSquareShape.html).
    ///
    /// * `data` - 2D vector of cells representing the shape to construct.
    ///
    /// # Examples
    ///
    /// ```
    /// use lyoko_solver::compound_square_shape::{CompoundSquareShape, Cell};
    ///
    /// let data = vec![
    ///     vec![Cell::Present, Cell::Present],
    ///     vec![Cell::Present, Cell::Present],
    /// ];
    /// let test = CompoundSquareShape::new(data);
    /// assert!(test.is_ok());
    /// ```
    pub fn new(data: Vec<Vec<Cell>>) -> Result<CompoundSquareShape, CompoundSquareShapeError> {
        if data.len() == 0 {
            return Err(CompoundSquareShapeError::Empty);
        }
        let height = data.len();
        let width = data[0].len();
        for row in &data {
            if row.len() == 0 {
                return Err(CompoundSquareShapeError::Empty);
            }
            if row.len() != width {
                return Err(CompoundSquareShapeError::NotSquare);
            }
        }

        return Ok(CompoundSquareShape { data, width, height });
    }

    /// Gets the width of this shape.
    pub fn get_width(&self) -> usize {
        return self.width;
    }

    /// Gets the height of this shape.
    pub fn get_height(&self) -> usize {
        return self.height;
    }

    /// Gets a reference to the underlying data of this shape.
    ///
    /// This is useful if you need to display the shape in some way.
    pub fn get_data(&self) -> &Vec<Vec<Cell>> {
        return &self.data;
    }

    /// Determines whether or not a [CompoundSquareShape](struct.CompoundSquareShape.html) is empty.
    ///
    /// Empty means that all of the component [Cell](enum.Cell.html)s are
    /// [Absent](enum.Cell.html#variant.Absent).
    pub fn is_empty(&self) -> bool {
        for row in &self.data {
            for cell in row {
                if *cell == Cell::Present {
                    return false;
                }
            }
        }

        return true;
    }

    /// Determines whether or not this fully contains another
    /// [CompoundSquareShape](struct.CompoundSquareShape.html).
    ///
    /// This means that every [Present](enum.Cell.html#variant.Present) [Cell](enum.Cell.html)
    /// of `other` is also [Present](enum.Cell.html#variant.Present) in this at the given offset.
    ///
    /// * `other` - The shape to check if this contains.
    /// * `offset` - The offset of `other` in this.
    ///
    /// # Examples
    ///
    /// ```
    /// use lyoko_solver::compound_square_shape::{CompoundSquareShape, Cell, Point2D};
    ///
    /// let data = vec![
    ///     vec![Cell::Present, Cell::Absent],
    ///     vec![Cell::Present, Cell::Present],
    /// ];
    /// let board = CompoundSquareShape::new(data).unwrap();
    ///
    /// let data = vec![
    ///     vec![Cell::Present],
    /// ];
    /// let piece = CompoundSquareShape::new(data).unwrap();
    ///
    /// assert!(board.contains(&piece, Point2D { x: 0, y: 0 }));
    /// assert!(!board.contains(&piece, Point2D { x: 1, y: 0 }));
    /// ```
    pub fn contains(&self, other: &CompoundSquareShape, offset: Point2D) -> bool {
        if other.width + offset.x > self.width {
            return false;
        }

        if other.height + offset.y > self.height {
            return false;
        }

        for (row_index, row) in other.data.iter().enumerate() {
            for (col_index, cell) in row.iter().enumerate() {
                if *cell == Cell::Present
                    && self.data[row_index + offset.y][col_index + offset.x] == Cell::Absent {
                    return false;
                }
            }
        }
        return true;
    }

    /// Generates a new [CompoundSquareShape](struct.CompoundSquareShape.html) equivalent to this
    /// with a shape "cut out" of it.
    ///
    /// The "cut" cells are [Absent](enum.Cell.html#variant.Absent).
    ///
    /// * `other` - The shape to cut out the shape of.
    /// * `offset` - The offset of `other` in this.
    ///
    /// # Examples
    ///
    /// ```
    /// use lyoko_solver::compound_square_shape::{CompoundSquareShape, Cell, Point2D};
    ///
    /// let data = vec![
    ///     vec![Cell::Present, Cell::Present],
    ///     vec![Cell::Present, Cell::Present],
    /// ];
    /// let board = CompoundSquareShape::new(data).unwrap();
    ///
    /// let data = vec![
    ///     vec![Cell::Present],
    ///     vec![Cell::Present],
    /// ];
    /// let piece = CompoundSquareShape::new(data).unwrap();
    ///
    /// let cut_board = board.cut(&piece, Point2D { x: 0, y: 0 });
    /// assert_eq!(cut_board.get_data()[0][0], Cell::Absent);
    /// assert_eq!(cut_board.get_data()[1][0], Cell::Absent);
    /// ```
    pub fn cut(&self, other: &CompoundSquareShape, offset: Point2D) -> CompoundSquareShape {
        let mut cut_data = self.data.clone();

        for (row_index, row) in other.data.iter().enumerate() {
            for (col_index, cell) in row.iter().enumerate() {
                if *cell == Cell::Present {
                    cut_data[row_index + offset.y][col_index + offset.x] = Cell::Absent;
                }
            }
        }

        return CompoundSquareShape{data: cut_data, width: self.width, height: self.height};
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_constructors() {
        let data = vec![];
        let test = CompoundSquareShape::new(data);
        assert!(test.is_err());

        let data = vec![
            vec![Cell::Present, Cell::Present],
            vec![Cell::Present],
        ];
        let test = CompoundSquareShape::new(data);
        assert!(test.is_err());

        let data = vec![
            vec![Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present],
            vec![],
        ];
        let test = CompoundSquareShape::new(data);
        assert!(test.is_err());

        let data = vec![
            vec![Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present],
        ];
        let test = CompoundSquareShape::new(data);
        assert!(test.is_ok());
        let test = test.unwrap();
        assert_eq!(test.get_width(), 2);
        assert_eq!(test.get_height(), 3);
    }
    
    #[test]
    fn test_empty() {
        let data = vec![
            vec![Cell::Present, Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present, Cell::Present],
        ];
        let test = CompoundSquareShape::new(data).unwrap();
        assert!(!test.is_empty());

        let data = vec![
            vec![Cell::Absent, Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present, Cell::Present],
        ];
        let test = CompoundSquareShape::new(data).unwrap();
        assert!(!test.is_empty());

        let data = vec![
            vec![Cell::Absent, Cell::Absent, Cell::Absent],
            vec![Cell::Absent, Cell::Present, Cell::Absent],
            vec![Cell::Absent, Cell::Absent, Cell::Absent],
        ];
        let test = CompoundSquareShape::new(data).unwrap();
        assert!(!test.is_empty());

        let data = vec![
            vec![Cell::Absent, Cell::Absent, Cell::Absent],
            vec![Cell::Absent, Cell::Absent, Cell::Absent],
            vec![Cell::Absent, Cell::Absent, Cell::Absent],
        ];
        let test = CompoundSquareShape::new(data).unwrap();
        assert!(test.is_empty());
    }

    #[test]
    fn test_contains_1() {
        // * *
        // * *
        let data = vec![
            vec![Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present],
        ];
        let board = CompoundSquareShape::new(data).unwrap();

        // *
        let data = vec![
            vec![Cell::Present],
        ];
        let piece = CompoundSquareShape::new(data).unwrap();

        assert!(board.contains(&piece, Point2D { x: 0, y: 0 }));
        assert!(board.contains(&piece, Point2D { x: 0, y: 1 }));
        assert!(board.contains(&piece, Point2D { x: 1, y: 0 }));
        assert!(board.contains(&piece, Point2D { x: 1, y: 1 }));
    }

    #[test]
    fn test_contains_2() {
        // * * * *
        // _ * * *
        // _ * * *
        // _ * * _
        // * * * *
        let data = vec![
            vec![Cell::Present, Cell::Present, Cell::Present, Cell::Present],
            vec![Cell::Absent, Cell::Present, Cell::Present, Cell::Present],
            vec![Cell::Absent, Cell::Present, Cell::Present, Cell::Present],
            vec![Cell::Absent, Cell::Present, Cell::Present, Cell::Absent],
            vec![Cell::Present, Cell::Present, Cell::Present, Cell::Present],
        ];
        let board = CompoundSquareShape::new(data).unwrap();

        // * *
        // _ *
        // _ *
        let data = vec![
            vec![Cell::Present, Cell::Present],
            vec![Cell::Absent, Cell::Present],
            vec![Cell::Absent, Cell::Present],
        ];
        let piece = CompoundSquareShape::new(data).unwrap();

        assert!(board.contains(&piece, Point2D { x: 0, y: 0 }));
        assert!(!board.contains(&piece, Point2D { x: 0, y: 1 }));
        assert!(!board.contains(&piece, Point2D { x: 0, y: 2 }));
        assert!(!board.contains(&piece, Point2D { x: 0, y: 3 }));
        assert!(!board.contains(&piece, Point2D { x: 0, y: 4 }));

        assert!(board.contains(&piece, Point2D { x: 1, y: 0 }));
        assert!(board.contains(&piece, Point2D { x: 1, y: 1 }));
        assert!(board.contains(&piece, Point2D { x: 1, y: 2 }));
        assert!(!board.contains(&piece, Point2D { x: 1, y: 3 }));
        assert!(!board.contains(&piece, Point2D { x: 1, y: 4 }));

        assert!(board.contains(&piece, Point2D { x: 2, y: 0 }));
        assert!(!board.contains(&piece, Point2D { x: 2, y: 1 }));
        assert!(!board.contains(&piece, Point2D { x: 2, y: 2 }));
        assert!(!board.contains(&piece, Point2D { x: 2, y: 3 }));
        assert!(!board.contains(&piece, Point2D { x: 2, y: 4 }));

        assert!(!board.contains(&piece, Point2D { x: 3, y: 0 }));
        assert!(!board.contains(&piece, Point2D { x: 3, y: 1 }));
        assert!(!board.contains(&piece, Point2D { x: 3, y: 2 }));
        assert!(!board.contains(&piece, Point2D { x: 3, y: 3 }));
        assert!(!board.contains(&piece, Point2D { x: 3, y: 4 }));

    }

    #[test]
    fn test_cut_1() {
        let data = vec![
            vec![Cell::Present, Cell::Present],
            vec![Cell::Present, Cell::Present],
        ];
        let board = CompoundSquareShape::new(data).unwrap();

        let data = vec![
            vec![Cell::Present],
            vec![Cell::Present],
        ];
        let piece = CompoundSquareShape::new(data).unwrap();

        let cut_board = board.cut(&piece, Point2D { x: 0, y: 0 });
        assert_eq!(cut_board.get_data()[0][0], Cell::Absent);
        assert_eq!(cut_board.get_data()[1][0], Cell::Absent);
    }
}
