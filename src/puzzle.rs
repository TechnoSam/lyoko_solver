//! Provides an interface for managing and displaying a Lyoko Puzzle.

use super::ansi;
use super::compound_square_shape as css;

use std::cmp;
use std::collections::HashMap;
use std::fmt;

/// Represents a Lyoko Puzzle.
#[derive(Clone, Debug)]
pub struct Puzzle {
    /// The board that the `pieces` should be fit into.
    pub board: css::CompoundSquareShape,
    /// The pieces to try to fit into the `board`.
    pub pieces: Vec<css::CompoundSquareShape>,
}

/// Errors that can result from displaying a [Puzzle](struct.Puzzle.html).
pub enum PuzzleDisplayError {
    /// More pieces to display than glyphs to display with.
    TooManyPieces,
    /// Fewer solution offsets than pieces.
    MismatchedSolution,
}

/// Minimum spacing between columns.
const COLUMN_SPACING: usize = 4;

/// Spacing between rows of pieces.
const VERTICAL_SPACING: usize = 1;

/// Prints a puzzle to stdout on the terminal, with color.
///
/// The board will be displayed in the middle with the pieces vertically on the left and right
/// sides.
///
/// * `puzzle` - The puzzle to display.
/// * `chars` - The ASCII characters to represent pieces with. There must be at least as many
/// characters as pieces in the `puzzle`.
/// * `colors` - The colors to color the pieces with. If there are less colors than pieces,
/// they will be reused.
///
/// # Examples
///
/// ```
/// use lyoko_solver::compound_square_shape::{CompoundSquareShape, Cell};
/// use lyoko_solver::puzzle::{Puzzle, display_unsolved};
/// use lyoko_solver::ansi::{Color, Fg, Bg};
///
/// let data = vec![
///     vec![Cell::Present, Cell::Absent],
///     vec![Cell::Present, Cell::Present],
/// ];
/// let board = CompoundSquareShape::new(data).unwrap();
///
/// let data = vec![
///     vec![Cell::Present],
/// ];
/// let piece = CompoundSquareShape::new(data).unwrap();
///
/// let mut pieces: Vec<CompoundSquareShape> = vec![];
///
/// pieces.push(piece);
///
/// let data = vec![
///    vec![Cell::Present, Cell::Present],
/// ];
/// let piece = CompoundSquareShape::new(data).unwrap();
///
/// pieces.push(piece);
///
/// let puzzle = Puzzle { board, pieces };
///
/// let chars = vec!['A', 'B', 'C', 'D'];
///
/// let colors = vec![Color { fg: Fg::White, bg: Bg::Red }];
///
/// let check = display_unsolved(&puzzle, &chars, &colors);
///
/// assert!(check.is_ok());
///
/// // Displays:
/// // ---- Input ---
/// //A |   0    |BB
/// //  |   00   |
/// ```
pub fn display_unsolved(
    puzzle: &Puzzle,
    chars: &[char],
    colors: &[ansi::Color],
) -> Result<(), PuzzleDisplayError> {
    if puzzle.pieces.len() > chars.len() {
        return Err(PuzzleDisplayError::TooManyPieces);
    }

    let mapper = PieceToPixelMapper::new(chars, colors);

    // Split the list of pieces into two halves
    let mut halfway_index = puzzle.pieces.len() / 2;
    if puzzle.pieces.len() % 2 == 1 {
        halfway_index += 1;
    }
    let left = &puzzle.pieces[0..halfway_index];
    let right = &puzzle.pieces[halfway_index..puzzle.pieces.len()];

    // Determine the needed width of the display
    let mut greatest_width = puzzle.board.get_width();
    for piece in &puzzle.pieces {
        if piece.get_width() > greatest_width {
            greatest_width = piece.get_width();
        }
    }

    let display_width = greatest_width
        + COLUMN_SPACING
        + puzzle.board.get_width()
        + COLUMN_SPACING
        + greatest_width;

    // Determine the needed height of the display
    let mut greatest_height = puzzle.board.get_height();
    let mut column_sum = 0usize;
    for piece in left {
        column_sum += piece.get_height() + VERTICAL_SPACING;
    }
    // Remove the last added space
    column_sum -= 1;
    if column_sum > greatest_height {
        greatest_height = column_sum;
    }
    column_sum = 0;
    for piece in right {
        column_sum += piece.get_height() + VERTICAL_SPACING;
    }
    // Remove the last added space
    column_sum -= 1;
    if column_sum > greatest_height {
        greatest_height = column_sum;
    }

    let display_height = greatest_height;

    let mut buffer = AsciiDisplayBuffer::new(display_width, display_height);

    // Draw the board in the buffer
    // The board is drawn in the second column
    let board_x_offset = greatest_width + COLUMN_SPACING;
    // The board is centered vertically
    let board_y_offset = display_height / 2 - puzzle.board.get_height() / 2;
    for (y, row) in puzzle.board.get_data().iter().enumerate() {
        for (x, cell) in row.iter().enumerate() {
            if *cell == css::Cell::Present {
                let board_pixel = AsciiDisplayPixel { value: '0', color: ansi::Color::new_n() };
                let check = buffer.set(x + board_x_offset, y + board_y_offset, board_pixel);
                match check {
                    Err(_) => panic!("Failed to write to ascii buffer"),
                    Ok(()) => (),
                }
            }
        }
    }

    // Draw the left pieces
    // The left half is drawn in the first column
    let left_x_offset = 0;
    // The first piece starts at the top
    let mut left_y_offset = 0;
    for (key, piece) in left.iter().enumerate() {
        for (y, row) in piece.get_data().iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                if *cell == css::Cell::Present {
                    // Center the piece horizontally in the column
                    let x_adjust = left_x_offset + (greatest_width - piece.get_width()) / 2;
                    let y_adjust = left_y_offset;
                    // Here, the key can be used to get the pixel directly, since it's from
                    // the first half of the list
                    let pixel = mapper.map(key);
                    let pixel = match pixel {
                        None => panic!("Failed to get pixel from piece map"),
                        Some(x) => x,
                    };
                    let check = buffer.set(x + x_adjust, y + y_adjust, pixel);
                    match check {
                        Err(_) => panic!("Failed to write to ascii buffer"),
                        Ok(()) => (),
                    }
                }
            }
        }
        // The next piece starts below
        left_y_offset += piece.get_height() + 1;
    }

    // Draw the right pieces
    // The right half is drawn in the third column
    let right_x_offset = greatest_width
        + COLUMN_SPACING
        + puzzle.board.get_width()
        + COLUMN_SPACING;
    // The first piece starts at the top
    let mut right_y_offset = 0;
    for (key, piece) in right.iter().enumerate() {
        for (y, row) in piece.get_data().iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                if *cell == css::Cell::Present {
                    // Center the piece horizontally in the column
                    let x_adjust = right_x_offset + (greatest_width - piece.get_width()) / 2;
                    let y_adjust = right_y_offset;
                    // Here, we need to add the halfway point to the key to get the pixel, since
                    // it's from the second half of the list
                    let pixel = mapper.map(key + halfway_index);
                    let pixel = match pixel {
                        None => panic!("Failed to get pixel from piece map"),
                        Some(x) => x,
                    };
                    let check = buffer.set(x + x_adjust, y + y_adjust, pixel);
                    match check {
                        Err(_) => panic!("Failed to write to ascii buffer"),
                        Ok(()) => (),
                    }
                }
            }
        }
        // The next piece starts below
        right_y_offset += piece.get_height() + 1;
    }

    // Draw lines dividing the pieces from the board
    let line_1_x = greatest_width;
    let line_2_x = greatest_width + COLUMN_SPACING + puzzle.board.get_width() + COLUMN_SPACING - 1;
    for y in 0..display_height {
        let check = buffer.set(line_1_x, y,
                               AsciiDisplayPixel { value: '|', color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
        let check = buffer.set(line_2_x, y,
                               AsciiDisplayPixel { value: '|', color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
    }

    let label = " Input ";
    let label_width = cmp::max(display_width, label.len());

    let label_start = label_width / 2 - label.len() / 2;
    let mut label_buffer = AsciiDisplayBuffer::new(label_width, 1);
    for label_index in 0..display_width {
        let check = label_buffer.set(label_index, 0,
                                     AsciiDisplayPixel { value: '-', color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
    }
    for (label_index, label_char) in label.chars().enumerate() {
        let check = label_buffer.set(label_start + label_index, 0,
                                     AsciiDisplayPixel { value: label_char,
                                         color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
    }

    print!("{}", label_buffer);
    println!("{}", buffer);

    return Ok(());
}

/// Prints a solved puzzle to stdout on the terminal, with color.
///
/// The filled in board will be displayed in the middle with the remaining pieces vertically on the
/// left and right sides in the same position as the unsolved configuration.
///
/// * `puzzle` - The puzzle to display.
/// * `sln` - Offsets of pieces within the board. The number of offsets must match the number of
/// pieces. The offset should be `None` if the corresponding piece is not in the solution.
/// * `chars` - The ASCII characters to represent pieces with. There must be at least as many
/// characters as pieces in the `puzzle`.
/// * `colors` - The colors to color the pieces with. If there are less colors than pieces,
/// they will be reused.
///
/// # Examples
///
/// ```
/// use lyoko_solver::compound_square_shape::{CompoundSquareShape, Cell, Point2D};
/// use lyoko_solver::puzzle::{Puzzle, display_solved};
/// use lyoko_solver::ansi::{Color, Fg, Bg};
///
/// let data = vec![
///     vec![Cell::Present, Cell::Absent],
///     vec![Cell::Present, Cell::Present],
/// ];
/// let board = CompoundSquareShape::new(data).unwrap();
///
/// let data = vec![
///     vec![Cell::Present],
/// ];
/// let piece = CompoundSquareShape::new(data).unwrap();
///
/// let mut pieces: Vec<CompoundSquareShape> = vec![];
///
/// pieces.push(piece);
///
/// let data = vec![
///    vec![Cell::Present, Cell::Present],
/// ];
/// let piece = CompoundSquareShape::new(data).unwrap();
///
/// pieces.push(piece);
///
/// let puzzle = Puzzle { board, pieces };
///
/// let sln: Vec<Option<Point2D>> = vec![Some(Point2D { x: 0, y: 0}), Some(Point2D { x: 0, y: 1})];
///
/// let chars = vec!['A', 'B', 'C', 'D'];
///
/// let colors = vec![Color { fg: Fg::White, bg: Bg::Red }];
///
/// let check = display_solved(&puzzle, &sln, &chars, &colors);
///
/// assert!(check.is_ok());
///
/// // Displays:
/// //-- Solution --
/// //  |   A    |
/// //  |   BB   |
/// ```
pub fn display_solved(
    puzzle: &Puzzle,
    sln: &Vec<Option<css::Point2D>>,
    chars: &[char],
    colors: &[ansi::Color],
) -> Result<(), PuzzleDisplayError> {
    if puzzle.pieces.len() > chars.len() {
        return Err(PuzzleDisplayError::TooManyPieces);
    }

    if puzzle.pieces.len() != sln.len() {
        return Err(PuzzleDisplayError::MismatchedSolution);
    }

    let mapper = PieceToPixelMapper::new(&chars, &colors);

    // Split the list of pieces into two halves
    let mut halfway_index = puzzle.pieces.len() / 2;
    if puzzle.pieces.len() % 2 == 1 {
        halfway_index += 1;
    }
    let left = &puzzle.pieces[0..halfway_index];
    let right = &puzzle.pieces[halfway_index..puzzle.pieces.len()];

    // Determine the needed width of the display
    let mut greatest_width = puzzle.board.get_width();
    for piece in &puzzle.pieces {
        if piece.get_width() > greatest_width {
            greatest_width = piece.get_width();
        }
    }

    let display_width = greatest_width
        + COLUMN_SPACING
        + puzzle.board.get_width()
        + COLUMN_SPACING
        + greatest_width;

    // Determine the needed height of the display
    let mut greatest_height = puzzle.board.get_height();
    let mut column_sum = 0usize;
    for piece in left {
        column_sum += piece.get_height() + VERTICAL_SPACING;
    }
    // Remove the last added space
    column_sum -= 1;
    if column_sum > greatest_height {
        greatest_height = column_sum;
    }
    column_sum = 0;
    for piece in right {
        column_sum += piece.get_height() + VERTICAL_SPACING;
    }
    // Remove the last added space
    column_sum -= 1;
    if column_sum > greatest_height {
        greatest_height = column_sum;
    }

    let display_height = greatest_height;

    let mut buffer = AsciiDisplayBuffer::new(display_width, display_height);

    // Draw the board in the buffer
    // The board is drawn in the second column
    let board_x_offset = greatest_width + COLUMN_SPACING;
    // The board is centered vertically
    let board_y_offset = display_height / 2 - puzzle.board.get_height() / 2;
    for (y, row) in puzzle.board.get_data().iter().enumerate() {
        for (x, cell) in row.iter().enumerate() {
            if *cell == css::Cell::Present {
                let board_pixel = AsciiDisplayPixel { value: '0', color: ansi::Color::new_n() };
                let check = buffer.set(x + board_x_offset, y + board_y_offset, board_pixel);
                match check {
                    Err(_) => panic!("Failed to write to ascii buffer"),
                    Ok(()) => (),
                }
            }
        }
    }

    // Draw the left pieces
    // The left half is drawn in the first column
    let left_x_offset = 0;
    // The first piece starts at the top
    let mut left_y_offset = 0;
    for (key, piece) in left.iter().enumerate() {
        let draw: bool;
        let offset_in_board = sln.get(key);
        let offset_in_board = match offset_in_board {
            None => panic!("Uncaught mismatched solution"),
            Some(x) => x,
        };
        match offset_in_board {
            None => draw = true,
            Some(_) => draw = false,
        };
        if draw {
            for (y, row) in piece.get_data().iter().enumerate() {
                for (x, cell) in row.iter().enumerate() {
                    if *cell == css::Cell::Present {
                        // Center the piece horizontally in the column
                        let x_adjust = left_x_offset + (greatest_width - piece.get_width()) / 2;
                        let y_adjust = left_y_offset;
                        // Here, the key can be used to get the pixel directly, since it's from
                        // the first half of the list
                        let pixel = mapper.map(key);
                        let pixel = match pixel {
                            None => panic!("Failed to get pixel from piece map"),
                            Some(x) => x,
                        };
                        let check = buffer.set(x + x_adjust, y + y_adjust, pixel);
                        match check {
                            Err(_) => panic!("Failed to write to ascii buffer"),
                            Ok(()) => (),
                        }
                    }
                }
            }
        }
        // The next piece starts below
        left_y_offset += piece.get_height() + 1;
    }

    // Draw the right pieces
    // The right half is drawn in the third column
    let right_x_offset = greatest_width
        + COLUMN_SPACING
        + puzzle.board.get_width()
        + COLUMN_SPACING;

    // The first piece starts at the top
    let mut right_y_offset = 0;
    for (key, piece) in right.iter().enumerate() {
        let draw: bool;
        let offset_in_board = sln.get(key + halfway_index);
        let offset_in_board = match offset_in_board {
            None => panic!("Uncaught mismatched solution"),
            Some(x) => x,
        };
        match offset_in_board {
            None => draw = true,
            Some(_) => draw = false,
        };
        if draw {
            for (y, row) in piece.get_data().iter().enumerate() {
                for (x, cell) in row.iter().enumerate() {
                    if *cell == css::Cell::Present {
                        // Center the piece horizontally in the column
                        let x_adjust = right_x_offset + (greatest_width - piece.get_width()) / 2;
                        let y_adjust = right_y_offset;
                        // Here, we need to add the halfway point to the key to get the pixel, since
                        // it's from the second half of the list
                        let pixel = mapper.map(key + halfway_index);
                        let pixel = match pixel {
                            None => panic!("Failed to get pixel from piece map"),
                            Some(x) => x,
                        };
                        let check = buffer.set(x + x_adjust, y + y_adjust, pixel);
                        match check {
                            Err(_) => panic!("Failed to write to ascii buffer"),
                            Ok(()) => (),
                        }
                    }
                }
            }
        }
        // The next piece starts below
        right_y_offset += piece.get_height() + 1;
    }

    // Draw the pieces
    for (key, piece) in puzzle.pieces.iter().enumerate() {
        for (y, row) in piece.get_data().iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                if *cell == css::Cell::Present {
                    let offset_in_board = sln.get(key);
                    let offset_in_board = match offset_in_board {
                        None => panic!("Uncaught mismatched solution"),
                        Some(x) => x,
                    };
                    let offset_in_board = match offset_in_board {
                        None => continue,
                        Some(x) => x,
                    };
                    let x_adjust = board_x_offset + offset_in_board.x;
                    let y_adjust = board_y_offset + offset_in_board.y;
                    let pixel = mapper.map(key);
                    let pixel = match pixel {
                        None => panic!("Failed to get pixel from piece map"),
                        Some(x) => x,
                    };
                    let check = buffer.set(x + x_adjust, y + y_adjust, pixel);
                    match check {
                        Err(_) => panic!("Failed to write to ascii buffer"),
                        Ok(()) => (),
                    }
                }
            }
        }
    }

    // Draw lines dividing the pieces from the board
    let line_1_x = greatest_width;
    let line_2_x = greatest_width + COLUMN_SPACING + puzzle.board.get_width() + COLUMN_SPACING - 1;
    for y in 0..display_height {
        let check = buffer.set(line_1_x, y,
                               AsciiDisplayPixel { value: '|', color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
        let check = buffer.set(line_2_x, y,
                               AsciiDisplayPixel { value: '|', color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
    }

    let label = " Solution ";
    let label_width = cmp::max(display_width, label.len());

    let label_start = label_width / 2 - label.len() / 2;
    let mut label_buffer = AsciiDisplayBuffer::new(label_width, 1);
    for label_index in 0..display_width {
        let check = label_buffer.set(label_index, 0,
                                     AsciiDisplayPixel { value: '-', color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
    }
    for (label_index, label_char) in label.chars().enumerate() {
        let check = label_buffer.set(label_start + label_index, 0,
                                     AsciiDisplayPixel { value: label_char,
                                         color: ansi::Color::new_n() });
        match check {
            Err(_) => panic!("Failed to write to ascii buffer"),
            Ok(()) => (),
        }
    }

    print!("{}", label_buffer);
    println!("{}", buffer);

    return Ok(());
}

/// Represents a "pixel" in an ASCII Display.
#[derive(Clone)]
struct AsciiDisplayPixel {
    /// The character to draw the pixel with.
    value: char,
    /// The color to draw the pixel with.
    color: ansi::Color,
}

/// A 2D Vector of [AsciiDisplayPixel](struct.AsciiDisplayPixel.html)s to edit before displaying.
struct AsciiDisplayBuffer {
    data: Vec<Vec<AsciiDisplayPixel>>,
}

/// Errors that can result from editing an [AsciiDisplayBuffer](struct.AsciiDisplayBuffer.html).
enum AsciiDisplayBufferError {
    /// Attempt to set an invalid index.
    IndexOutOfBounds,
}

impl AsciiDisplayBuffer {
    /// Constructs a new [AsciiDisplayBuffer](struct.AsciiDisplayBuffer.html).
    ///
    /// * `width` - The width of the buffer.
    /// * `height` - the height of the buffer.
    pub fn new(width: usize, height: usize) -> AsciiDisplayBuffer {
        let mut data: Vec<Vec<AsciiDisplayPixel>> = vec![];
        for _h in 0..height {
            let mut row = vec![];
            for _w in 0..width {
                row.push(AsciiDisplayPixel { value: ' ', color: ansi::Color::new_n()});
            }
            data.push(row);
        }

        return AsciiDisplayBuffer { data };
    }

    /// Sets a pixel in the buffer.
    ///
    /// * `x` - The x coordinate of the pixel to set.
    /// * `y` - The y coordinate of the pixel to set.
    /// * `pixel` - The pixel to set.
    pub fn set(
        &mut self,
        x: usize,
        y: usize,
        pixel: AsciiDisplayPixel,
    ) -> Result<(), AsciiDisplayBufferError> {
        let row = self.data.get_mut(y);
        let row = match row {
            None => return Err(AsciiDisplayBufferError::IndexOutOfBounds),
            Some(x) => x,
        };
        let prev = row.get_mut(x);
        let prev = match prev {
            None => return Err(AsciiDisplayBufferError::IndexOutOfBounds),
            Some(x) => x,
        };
        *prev = pixel;

        return Ok(());
    }

}

impl fmt::Display for AsciiDisplayBuffer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in &self.data {
            for pixel in row {
                write!(f, "{}{}{}", pixel.color.set(), pixel.value, pixel.color.reset())?;
            }
            write!(f, "\n")?;
        }

        return Ok(());
    }
}

/// Manages mapping Puzzle pieces to pixels.
///
/// This makes it easy to display pieces with a consistent style.
struct PieceToPixelMapper {
    map: HashMap<usize, AsciiDisplayPixel>,
}

impl PieceToPixelMapper {
    /// Constructs a new [PieceToPixelMapper](struct.PieceToPixelMapper.html).
    ///
    /// * `characters` - Available characters to map to.
    /// * `colors` - Available colors to map to.
    pub fn new(characters: &[char], colors: &[ansi::Color]) -> PieceToPixelMapper {

        let mut map: HashMap<usize, AsciiDisplayPixel> = HashMap::new();
        let mut color_index = 0;

        for (key, character) in characters.iter().enumerate() {
            let color = colors.get(color_index);
            let color = match color {
                None => ansi::Color { fg: ansi::Fg::None, bg: ansi::Bg::None },
                Some(x) => x.clone(),
            };
            color_index = (color_index + 1) % colors.len();
            map.insert(key, AsciiDisplayPixel { value: character.clone(), color});
        }

        return PieceToPixelMapper { map };
    }

    /// Returns an owned pixel for the piece index. If the index is not in the map, a `None` will
    /// be returned.
    ///
    /// * `index` - The index of the piece to get the pixel for.
    pub fn map(&self, index: usize) -> Option<AsciiDisplayPixel> {
        return self.map.get(&index).cloned();
    }
}
