//! Provides structures and functions to manage parsing, solving, and displaying Lyoko Puzzles.

pub mod ansi;
pub mod compound_square_shape;
pub mod compound_square_shape_file_parser;
pub mod puzzle;

use compound_square_shape as css;
use compound_square_shape_file_parser as cssfp;

/// Attempts to solve a Lyoko Puzzle.
///
/// * `board` - Shape that represents the board.
/// * `pieces` - Slice of shapes that represent the pieces.
/// * `returns` - Optional vector of optional points. If the vector is None, no solution was found.
/// Otherwise, the vector will contain a point for each piece. If the point is None, the
/// corresponding piece is not used in the solution. Otherwise the point is the offset in the board
/// where the corresponding piece belongs in the solution.
pub fn solve(board: &css::CompoundSquareShape, pieces: &[css::CompoundSquareShape])
    -> Option<Vec<Option<css::Point2D>>> {
    // Base case
    if pieces.len() == 0 {
        // If the board is empty, that means it has been completely filled in,
        // so we have a solution.
        if board.is_empty() {
            return Some(vec![]);
        }
        // If the board isn't empty, this set of pieces was unable to fill the board,
        // so it isn't a solution.
        else {
            return None;
        }
    }

    // Search for a home for the next piece
    for x in 0..board.get_width() {
        for y in 0..board.get_height() {
            if board.contains(&pieces[0], css::Point2D { x, y }) {
                let cut_board = board.cut(&pieces[0], css::Point2D { x, y });
                let slns = solve(&cut_board, &pieces[1..]);
                let mut slns = match slns {
                    // If we didn't find a solution in this configuration,
                    // we should try an alternate location for this piece.
                    None => {
                        continue;
                    },
                    Some(x) => x,
                };
                slns.insert(0, Some(css::Point2D { x, y }));
                return Some(slns);
            }
        }
    }

    // If we couldn't find a home, try leaving it out
    let slns = solve(&board, &pieces[1..]);
    let mut slns = match slns {
        None => return None,
        Some(x) => x,
    };
    slns.insert(0, None);
    return Some(slns);
}

/// Parses a Lyoko Puzzle file (defined in compound_square_shape_parser) into a Puzzle that can be
/// used for displaying or solving.
///
/// * `file_name` - The name of the file to parse.
pub fn file_to_puzzle(file_name: &str) -> Result<puzzle::Puzzle, cssfp::CssfpError> {
    let mut pieces = cssfp::parse(file_name)?;

    let board = pieces.remove(0);

    return Ok(puzzle::Puzzle { board, pieces });
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_1() {
        // * * *
        // * * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
        ];
        let board = css::CompoundSquareShape::new(data).unwrap();

        // *
        // *
        let data = vec![
            vec![css::Cell::Present],
            vec![css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();

        let mut pieces = vec![piece];

        // * *
        // * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        let slns = solve(&board, &pieces);

        assert!(slns.is_some());

        let slns = slns.unwrap();

        assert_eq!(pieces.len(), 2);
        assert_eq!(slns.len(), pieces.len());

        assert!(slns[0].is_some());
        let test = slns[0].clone().unwrap();
        assert_eq!(test.x, 0);
        assert_eq!(test.y, 0);

        assert!(slns[1].is_some());
        let test = slns[1].clone().unwrap();
        assert_eq!(test.x, 1);
        assert_eq!(test.y, 0);
    }

    #[test]
    fn test_solve_2() {
        // _ * _ _ * _
        // * * * * * *
        // _ _ * * * _
        // * * * * * *
        let data = vec![
            vec![
                css::Cell::Absent,
                css::Cell::Present,
                css::Cell::Absent,
                css::Cell::Absent,
                css::Cell::Present,
                css::Cell::Absent,
            ],
            vec![
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
            ],
            vec![
                css::Cell::Absent,
                css::Cell::Absent,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Absent,
            ],
            vec![
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
            ],
        ];
        let board = css::CompoundSquareShape::new(data).unwrap();

        // _ * _
        // * * *
        let data = vec![
            vec![css::Cell::Absent, css::Cell::Present, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();

        let mut pieces = vec![piece];

        // * _
        // * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // * _ _
        // * * _
        // * * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Absent, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // _ *
        // * *
        let data = vec![
            vec![css::Cell::Absent, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // *
        let data = vec![vec![css::Cell::Present]];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        let slns = solve(&board, &pieces);

        assert!(slns.is_some());

        let slns = slns.unwrap();

        assert_eq!(pieces.len(), 5);
        assert_eq!(slns.len(), pieces.len());

        assert!(slns[0].is_some());
        let test = slns[0].clone().unwrap();
        assert_eq!(test.x, 0);
        assert_eq!(test.y, 0);

        assert!(slns[1].is_some());
        let test = slns[1].clone().unwrap();
        assert_eq!(test.x, 4);
        assert_eq!(test.y, 0);

        assert!(slns[2].is_some());
        let test = slns[2].clone().unwrap();
        assert_eq!(test.x, 3);
        assert_eq!(test.y, 1);

        assert!(slns[3].is_some());
        let test = slns[3].clone().unwrap();
        assert_eq!(test.x, 1);
        assert_eq!(test.y, 2);

        assert!(slns[4].is_some());
        let test = slns[4].clone().unwrap();
        assert_eq!(test.x, 0);
        assert_eq!(test.y, 3);
    }

    #[test]
    fn test_solve_3() {
        // _ * _ _ * _
        // * * * * * *
        // _ _ * * * _
        // * * * * * *
        let data = vec![
            vec![
                css::Cell::Absent,
                css::Cell::Present,
                css::Cell::Absent,
                css::Cell::Absent,
                css::Cell::Present,
                css::Cell::Absent,
            ],
            vec![
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
            ],
            vec![
                css::Cell::Absent,
                css::Cell::Absent,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Absent,
            ],
            vec![
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
                css::Cell::Present,
            ],
        ];
        let board = css::CompoundSquareShape::new(data).unwrap();

        // Not used
        // * *
        // _ *
        // * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Present],
            vec![css::Cell::Absent, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();

        let mut pieces = vec![piece];

        // _ * _
        // * * *
        let data = vec![
            vec![css::Cell::Absent, css::Cell::Present, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // * _
        // * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // Not used
        // * * *
        // * * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // * _ _
        // * * _
        // * * *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Absent, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Absent],
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // _ *
        // * *
        let data = vec![
            vec![css::Cell::Absent, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // Not used (duplicate)
        // _ *
        // * *
        let data = vec![
            vec![css::Cell::Absent, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Present],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // *
        let data = vec![vec![css::Cell::Present]];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        // Not used
        // * * *
        // *
        let data = vec![
            vec![css::Cell::Present, css::Cell::Present, css::Cell::Present],
            vec![css::Cell::Present, css::Cell::Absent, css::Cell::Absent],
        ];
        let piece = css::CompoundSquareShape::new(data).unwrap();
        pieces.push(piece);

        let slns = solve(&board, &pieces);

        assert!(slns.is_some());

        let slns = slns.unwrap();

        assert_eq!(pieces.len(), 9);
        assert_eq!(slns.len(), pieces.len());

        assert!(slns[0].is_none());

        assert!(slns[1].is_some());
        let test = slns[1].clone().unwrap();
        assert_eq!(test.x, 0);
        assert_eq!(test.y, 0);

        assert!(slns[2].is_some());
        let test = slns[2].clone().unwrap();
        assert_eq!(test.x, 4);
        assert_eq!(test.y, 0);

        assert!(slns[3].is_none());

        assert!(slns[4].is_some());
        let test = slns[4].clone().unwrap();
        assert_eq!(test.x, 3);
        assert_eq!(test.y, 1);

        assert!(slns[5].is_some());
        let test = slns[5].clone().unwrap();
        assert_eq!(test.x, 1);
        assert_eq!(test.y, 2);

        assert!(slns[6].is_none());

        assert!(slns[7].is_some());
        let test = slns[7].clone().unwrap();
        assert_eq!(test.x, 0);
        assert_eq!(test.y, 3);

        assert!(slns[8].is_none());
    }
}
