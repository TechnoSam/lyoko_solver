//! Provides a simple interface for getting the set and reset control codes
//! for coloring text in a terminal emulator output.

use std::str::FromStr;

/// Represents a foreground color.
#[derive(Clone, PartialEq)]
pub enum Fg {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    BrightBlack,
    BrightRed,
    BrightGreen,
    BrightYellow,
    BrightBlue,
    BrightMagenta,
    BrightCyan,
    BrightWhite,
    /// Default color
    None,
}

impl Fg {
    /// Gets the appropriate control code for the color.
    fn value(&self) -> &str {
        match *self {
            Fg::Black => "30",
            Fg::Red => "31",
            Fg::Green => "32",
            Fg::Yellow => "33",
            Fg::Blue => "34",
            Fg::Magenta => "35",
            Fg::Cyan => "36",
            Fg::White => "37",
            Fg::BrightBlack => "30;1",
            Fg::BrightRed => "31;1",
            Fg::BrightGreen => "32;1",
            Fg::BrightYellow => "33;1",
            Fg::BrightBlue => "34;1",
            Fg::BrightMagenta => "35;1",
            Fg::BrightCyan => "36;1",
            Fg::BrightWhite => "37;1",
            Fg::None => "",
        }
    }
}

impl FromStr for Fg {
    type Err = ();

    fn from_str(s: &str) -> Result<Fg, ()> {
        match s {
            "Black" => Ok(Fg::Black),
            "Red" => Ok(Fg::Red),
            "Green" => Ok(Fg::Green),
            "Yellow" => Ok(Fg::Yellow),
            "Blue" => Ok(Fg::Blue),
            "Magenta" => Ok(Fg::Magenta),
            "Cyan" => Ok(Fg::Cyan),
            "White" => Ok(Fg::White),
            "BrightBlack" => Ok(Fg::BrightBlack),
            "BrightRed" => Ok(Fg::BrightRed),
            "BrightGreen" => Ok(Fg::BrightGreen),
            "BrightYellow" => Ok(Fg::BrightYellow),
            "BrightBlue" => Ok(Fg::BrightBlue),
            "BrightMagenta" => Ok(Fg::BrightMagenta),
            "BrightCyan" => Ok(Fg::BrightCyan),
            "BrightWhite" => Ok(Fg::BrightWhite),
            "None" => Ok(Fg::None),
            _ => Err(()),
        }
    }
}

/// Represents a background color.
#[derive(Clone, PartialEq)]
pub enum Bg {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    BrightBlack,
    BrightRed,
    BrightGreen,
    BrightYellow,
    BrightBlue,
    BrightMagenta,
    BrightCyan,
    BrightWhite,
    /// Default color
    None,
}

impl Bg {
    /// Gets the appropriate control code for the color.
    fn value(&self) -> &str {
        match *self {
            Bg::Black => "40",
            Bg::Red => "41",
            Bg::Green => "42",
            Bg::Yellow => "43",
            Bg::Blue => "44",
            Bg::Magenta => "45",
            Bg::Cyan => "46",
            Bg::White => "47",
            Bg::BrightBlack => "100",
            Bg::BrightRed => "101",
            Bg::BrightGreen => "102",
            Bg::BrightYellow => "103",
            Bg::BrightBlue => "104",
            Bg::BrightMagenta => "105",
            Bg::BrightCyan => "106",
            Bg::BrightWhite => "107",
            Bg::None => "",
        }
    }
}

impl FromStr for Bg {
    type Err = ();

    fn from_str(s: &str) -> Result<Bg, ()> {
        match s {
            "Black" => Ok(Bg::Black),
            "Red" => Ok(Bg::Red),
            "Green" => Ok(Bg::Green),
            "Yellow" => Ok(Bg::Yellow),
            "Blue" => Ok(Bg::Blue),
            "Magenta" => Ok(Bg::Magenta),
            "Cyan" => Ok(Bg::Cyan),
            "White" => Ok(Bg::White),
            "BrightBlack" => Ok(Bg::BrightBlack),
            "BrightRed" => Ok(Bg::BrightRed),
            "BrightGreen" => Ok(Bg::BrightGreen),
            "BrightYellow" => Ok(Bg::BrightYellow),
            "BrightBlue" => Ok(Bg::BrightBlue),
            "BrightMagenta" => Ok(Bg::BrightMagenta),
            "BrightCyan" => Ok(Bg::BrightCyan),
            "BrightWhite" => Ok(Bg::BrightWhite),
            "None" => Ok(Bg::None),
            _ => Err(()),
        }
    }
}

/// Represents a full terminal color style (background and foreground) and provides the appropriate
/// set and reset ANSI strings.
#[derive(Clone)]
pub struct Color {
    /// The foreground color.
    pub fg: Fg,
    /// The background color.
    pub bg: Bg,
}

impl Color {
    /// Constructs a new [Color](struct.Color.html).
    ///
    /// * `fg` - The foreground color.
    /// * `bg` - The background color.
    pub fn new(fg: Fg, bg: Bg) -> Color {
        return Color { fg, bg };
    }

    /// Constructs a new [Color](struct.Color.html) with default background.
    ///
    /// * `fg` - The foreground color.
    pub fn new_f(fg: Fg) -> Color {
        return Color { fg, bg: Bg::None };
    }

    /// Constructs a new [Color](struct.Color.html) with default foreground.
    ///
    /// * `bg` - The background color.
    pub fn new_b(bg: Bg) -> Color {
        return Color { fg: Fg::None, bg };
    }

    /// Constructs a new [Color](struct.Color.html) with default foreground and background.
    pub fn new_n() -> Color {
        return Color { fg: Fg::None, bg: Bg::None };
    }

    /// Returns the ANSI set sequence for this.
    pub fn set(&self) -> String {
        if self.fg == Fg::None && self.bg == Bg::None {
            return String::from("");
        }

        return format!("\u{001b}[{};{}m", self.fg.value(), self.bg.value());
    }

    /// Returns the ANSI reset sequence for this.
    pub fn reset(&self) -> String {
        if self.fg == Fg::None && self.bg == Bg::None {
            return String::from("");
        }

        return format!("\u{001b}[0m");
    }
}
