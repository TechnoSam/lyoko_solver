//! Used for parsing files to
//! [CompoundSquareShape](compound_square_shape/struct.CompoundSquareShape.html)s.
//!
//! # File Format
//!
//! The "Lyoko Puzzle" (`*.lp`) file format obeys the following rules.
//!
//! * Shapes are defined in rows of ASCII characters. Each row represents a row of the shape.
//! A '.' character means the
//! [Cell](../compound_square_shape/enum.Cell.html) is
//! [Absent](../compound_square_shape/enum.Cell.html#variant.Absent). Any other non-control,
//! non-whitespace character means the Cell is
//! [Present](../compound_square_shape/enum.Cell.html#variant.Present).
//!
//! * Shapes must be valid. See
//! [CompoundSquareShapeError](../compound_square_shape/enum.CompoundSquareShapeError.html).
//!
//! * Multiple shapes can be defined. An empty line separates two shapes.
//!
//! Refer to the provided example files if needed.

use super::compound_square_shape as css;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

/// Errors that can occur while parsing a Lyoko Puzzle file.
#[derive(Debug, PartialEq)]
pub enum CssfpError {
    /// The file requested does not exist.
    FileNotFound,
    /// The file exists, but could not be read.
    FileMalformed,
    /// Non-ASCII data found in file.
    DataNonAscii,
    /// A parsed shape failed to be constructed. See CompoundSquareShapeError.
    PuzzleMalformed,
}

/// States for the parsing state machine.
#[derive(Clone, Copy, PartialEq)]
enum State {
    /// Initial state, no shape is currently being parsed.
    Start,
    /// Currently parsing a row.
    Row,
    /// We have just finished a row, the shape may be done or it may go on.
    RowDone,
}

/// Parses a file into CompoundSquareShapes
///
/// * `file_name` - Path to the file to parse.
pub fn parse(file_name: &str) -> Result<Vec<css::CompoundSquareShape>, CssfpError> {
    let file = File::open(file_name);
    let file = match file {
        Err(_) => {
            return Err(CssfpError::FileNotFound);
        }
        Ok(x) => x,
    };

    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    let check = buf_reader.read_to_string(&mut contents);
    match check {
        Err(_) => {
            return Err(CssfpError::FileMalformed);
        }
        Ok(_) => {}
    }

    return parse_string(&contents);
}

/// Parses a string into CompoundSquareShapes
///
/// * `shapes_str` - The string to parse.
fn parse_string(shapes_str: &str) -> Result<Vec<css::CompoundSquareShape>, CssfpError> {
    let mut shapes: Vec<css::CompoundSquareShape> = vec![];
    let mut data: Vec<Vec<css::Cell>> = vec![];

    let mut state = State::Start;
    let mut next_state;

    for character in shapes_str.chars() {
        if !character.is_ascii() {
            return Err(CssfpError::DataNonAscii);
        }

        // State transitions
        match state {
            State::Start => {
                if character.is_whitespace() || character.is_control() {
                    next_state = State::Start;
                }
                else {
                    next_state = State::Row;
                    // Create the first row
                    data.push(vec![]);
                }
            }
            State::Row => {
                if character == '\n' {
                    next_state = State::RowDone;
                }
                else {
                    next_state = State::Row;
                }
            }
            State::RowDone => {
                if character == '\n' {
                    // This shape is fully parsed, try to create a CompoundSquareShape.
                    next_state = State::Start;
                    let data_clone = data.clone();
                    let shape = css::CompoundSquareShape::new(data_clone);
                    let shape = match shape {
                        Err(_) => {
                            return Err(CssfpError::PuzzleMalformed);
                        }
                        Ok(x) => x,
                    };
                    data.clear();
                    shapes.push(shape);
                }
                else {
                    next_state = State::Row;
                    // Create the next row
                    data.push(vec![]);
                }
            }
        }

        state = next_state;

        // State actions - Handle the read character based on the current state
        match state {
            State::Start => {}
            State::Row => {
                let curr_row = data.last_mut();
                let curr_row = match curr_row {
                    None => {
                        panic!("State Machine Broke");
                    }
                    Some(x) => x,
                };
                curr_row.push(if character == '.' {
                    css::Cell::Absent
                }
                else {
                    css::Cell::Present
                });
            }
            State::RowDone => {}
        }
    }

    // The text may not have ended in a newline as is typical, so we should try to parse
    // a shape out of any data we have buffered.
    if !data.is_empty() {
        let data_clone = data.clone();
        let shape = css::CompoundSquareShape::new(data_clone);
        let shape = match shape {
            Err(_) => {
                return Err(CssfpError::PuzzleMalformed);
            }
            Ok(x) => x,
        };
        data.clear();
        shapes.push(shape);
    }

    return Ok(shapes);
}

#[cfg(test)]
mod tests {

    use super::*;
    use std::fs;

    #[test]
    fn test_1_shape() {
        let shapes_str = "\
            ..OO..\n\
            OOOOOO\n\
            ..OO..\n";

        let test = parse_string(shapes_str);

        assert!(test.is_ok());
        let test = test.unwrap();
        assert_eq!(test.len(), 1);

        assert_eq!(test[0].get_data()[0][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][5], css::Cell::Absent);
        assert_eq!(test[0].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][1], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][4], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][5], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][5], css::Cell::Absent);
    }

    #[test]
    fn test_1_shape_file() {
        let shapes_str = "\
            ..OO..\n\
            OOOOOO\n\
            ..OO..\n";

        let file_name = "test.lp";

        let mut file = File::create(file_name).unwrap();
        let check = file.write_all(shapes_str.as_bytes());
        match check {
            Err(_) => assert!(false),
            Ok(_) => {}
        }

        let test = parse(file_name);

        assert!(test.is_ok());
        let test = test.unwrap();
        assert_eq!(test.len(), 1);

        assert_eq!(test[0].get_data()[0][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][5], css::Cell::Absent);
        assert_eq!(test[0].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][1], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][4], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][5], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][5], css::Cell::Absent);

        let check = fs::remove_file(file_name);
        assert!(check.is_ok());

        let test = parse("test.lpf");
        assert!(test.is_err());
        let test = test.err().unwrap();
        assert_eq!(test, CssfpError::FileNotFound);
    }

    #[test]
    fn test_4_shapes() {
        let shapes_str = "\
            ..OO..\n\
            OOOOOO\n\
            ..OO..\n\
            \n\
            OO\n\
            \n\
            OOO\n\
            ..O\n\
            \n\
            OOO\n\
            O..\n";

        let test = parse_string(shapes_str);

        assert!(test.is_ok());
        let test = test.unwrap();
        assert_eq!(test.len(), 4);

        assert_eq!(test[0].get_data()[0][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][5], css::Cell::Absent);
        assert_eq!(test[0].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][1], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][4], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][5], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][5], css::Cell::Absent);

        assert_eq!(test[1].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[1].get_data()[0][1], css::Cell::Present);

        assert_eq!(test[2].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[2].get_data()[0][1], css::Cell::Present);
        assert_eq!(test[2].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[2].get_data()[1][0], css::Cell::Absent);
        assert_eq!(test[2].get_data()[1][1], css::Cell::Absent);
        assert_eq!(test[2].get_data()[1][2], css::Cell::Present);

        assert_eq!(test[3].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[3].get_data()[0][1], css::Cell::Present);
        assert_eq!(test[3].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[3].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[3].get_data()[1][1], css::Cell::Absent);
        assert_eq!(test[3].get_data()[1][2], css::Cell::Absent);
    }

    #[test]
    fn test_4_shapes_extraneous_whitespace() {
        let shapes_str = "\
            \n\n  \n
            ..OO..\n\
            OOOOOO\n\
            ..OO..\n\
            \n\
            \n\n
            OO\n\
            \n\
            OOO\n\
            ..O\n\
            \n\
            OOO\n\
            O..\n\
            \n\n    \n\n";

        let test = parse_string(shapes_str);

        assert!(test.is_ok());
        let test = test.unwrap();
        assert_eq!(test.len(), 4);

        assert_eq!(test[0].get_data()[0][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][5], css::Cell::Absent);
        assert_eq!(test[0].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][1], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][4], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][5], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][5], css::Cell::Absent);

        assert_eq!(test[1].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[1].get_data()[0][1], css::Cell::Present);

        assert_eq!(test[2].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[2].get_data()[0][1], css::Cell::Present);
        assert_eq!(test[2].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[2].get_data()[1][0], css::Cell::Absent);
        assert_eq!(test[2].get_data()[1][1], css::Cell::Absent);
        assert_eq!(test[2].get_data()[1][2], css::Cell::Present);

        assert_eq!(test[3].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[3].get_data()[0][1], css::Cell::Present);
        assert_eq!(test[3].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[3].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[3].get_data()[1][1], css::Cell::Absent);
        assert_eq!(test[3].get_data()[1][2], css::Cell::Absent);
    }

    #[test]
    fn test_4_shapes_mixed_chars() {
        let shapes_str = "\
            \n\n  \n
            ..AB..\n\
            CDEFGH\n\
            ..JI..\n\
            \n\
            \n\n
            ab\n\
            \n\
            123\n\
            ..4\n\
            \n\
            ###\n\
            #..\n\
            \n\n    \n\n";

        let test = parse_string(shapes_str);

        assert!(test.is_ok());
        let test = test.unwrap();
        assert_eq!(test.len(), 4);

        assert_eq!(test[0].get_data()[0][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[0][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[0][5], css::Cell::Absent);
        assert_eq!(test[0].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][1], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][4], css::Cell::Present);
        assert_eq!(test[0].get_data()[1][5], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][0], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][1], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][2], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][3], css::Cell::Present);
        assert_eq!(test[0].get_data()[2][4], css::Cell::Absent);
        assert_eq!(test[0].get_data()[2][5], css::Cell::Absent);

        assert_eq!(test[1].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[1].get_data()[0][1], css::Cell::Present);

        assert_eq!(test[2].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[2].get_data()[0][1], css::Cell::Present);
        assert_eq!(test[2].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[2].get_data()[1][0], css::Cell::Absent);
        assert_eq!(test[2].get_data()[1][1], css::Cell::Absent);
        assert_eq!(test[2].get_data()[1][2], css::Cell::Present);

        assert_eq!(test[3].get_data()[0][0], css::Cell::Present);
        assert_eq!(test[3].get_data()[0][1], css::Cell::Present);
        assert_eq!(test[3].get_data()[0][2], css::Cell::Present);
        assert_eq!(test[3].get_data()[1][0], css::Cell::Present);
        assert_eq!(test[3].get_data()[1][1], css::Cell::Absent);
        assert_eq!(test[3].get_data()[1][2], css::Cell::Absent);
    }

    #[test]
    fn test_non_ascii() {
        let shapes_str = "\
            ..αα..\n\
            αααααα\n\
            ..αα..\n";

        let test = parse_string(shapes_str);
        assert!(test.is_err());
        let test = test.err().unwrap();
        assert_eq!(test, CssfpError::DataNonAscii);
    }

    #[test]

    fn test_bad_shape() {
        let shapes_str = "\
            ..OO..\n\
            OOOOOO\n\
            ..OO..\n\
            \n\
            OO\n\
            \n\
            OOO\n\
            ..OO\n\
            \n\
            OOO\n\
            O..\n";

        let test = parse_string(shapes_str);
        assert!(test.is_err());
        let test = test.err().unwrap();
        assert_eq!(test, CssfpError::PuzzleMalformed);
    }

}
