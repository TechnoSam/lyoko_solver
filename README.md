# Lyoko Solver

This program solves the "hacking" puzzles in the Nintendo DS game,
[Code Lyoko: Get Ready to Virtualize](http://en.codelyoko.fr/jeuxvideo/jeuds.cl).
These puzzles appear when Aelita deactivates a Tower at the end of each
level and more complex puzzles are available via the main menu.

The puzzles are basically games of
[Tangrams](https://en.wikipedia.org/wiki/Tangram), but simplified in
that each shape is made up of discrete squares, rather than general
polygons.

![Puzzle 3](resources/puzzle_3.png)

# Building the program

This program is written in [Rust](https://www.rust-lang.org/en-US/).

Installing Rust is simple. Follow the instructions on
[this page](https://www.rust-lang.org/en-US/install.html).

To build, just open a terminal in this directory, and run `cargo build`.
The executable can be found in `target/debug`.

The program has been tested in macOS, Windows, and Debian.

# Usage

```
USAGE:
    lyoko_solver [OPTIONS] <PUZZLE_FILE>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --config <FILE>    Sets a custom config file

ARGS:
    <PUZZLE_FILE>    Sets the puzzle file to use
```

## Configuration

`lyoko_solver` accepts a config file that controls aspects of its
behavior. You can see an example with sensible values at `config.toml`.

The config file is a [TOML](https://github.com/toml-lang/toml) file
with only one key: `colors`. It is an array of inline tables with
two optional keys, `fg` and `bg`. Both keys accept the following
possible values (as strings):

* Black
* Red
* Green
* Yellow
* Blue
* Magenta
* Cyan
* White
* BrightBlack
* BrightRed
* BrightGreen
* BrightYellow
* BrightBlue
* BrightMagenta
* BrightCyan
* BrightWhite
* None (Default color)

If either value is not provided, it is defaulted to "None".

These values correspond to [ANSI color codes](https://en.wikipedia.org/wiki/ANSI_escape_code#Colors).

## Puzzle Files

`lyoko_solver` accepts files of the "Lyoko Puzzle" (`*.lp`) format.
You can find all the puzzles from the game in the `puzzles` directory.
If you want to make your own, refer to the official documentation,
`compound_square_shape_file_parser`.

You can generate this yourself with `cargo doc --open`. If I can include
this on GitLab I would like to.

# Complaints

This is the first "real" project I've done in Rust, so I'm logging
my complaints here, so I can remember to ask for advice.

---

My biggest question project organization. Every language is a bit
different here, and I don't know if what I'm doing is the preferred
idiomatic organization.

---

How can I ensure that a struct is constructed in a good state?
It seems like any library user could simply create it with {} and
have invalid data, such as width, height, or with invalid
guaranteed bounds.

---

I'm still not certain that I understand borrows and references
thoroughly. There are several instances that I call `clone`, and some
make sense, but I'm not sure that all do. Have I done anything dumb
here?

For a specific example:

In main, is cloning the puzzle good style? It needs to get captured in
the lambda, but I don't want to move it, because I need it again to
display the solution. It seems like I should be able to pass a reference
to the lambda, but I couldn't figure out how.

---

`rustfmt` seems to be unusable at the moment. Several issues here.

1. Several important options (like `brace_style`) are not stable. This
leads to situations like this:

```
         for (row_index, row) in other.data.iter().enumerate() {
             for (col_index, cell) in row.iter().enumerate() {
-                if *cell == Cell::Present && self.data[row_index + offset.y][col_index + offset.x] == Cell::Absent {
+                if *cell == Cell::Present
+                    && self.data[row_index + offset.y][col_index + offset.x] == Cell::Absent
+                {
                     return false;
                 }
             }
```

I don't want the brace on the new line, but I can't fix that with stable
features.

2. Inability to format a selection of files with `cargo fmt`

This isn't a huge deal, but it is kind of pain to be railroaded into
formatting the entire project.

3. No "explain" annotations, showing why formatting changes are made.

This is essential for seeing exactly what spec the old formatting does
not conform to, which you need to know if you want to change it or
read the full spec to understand the rationale and adjust your style.

4. The indent style should be block by default, but it seems like it's
using visual.

5. Similar deal, I don't like the

```
if (condition) {
    actions
} else {
    action
}
```

format. Is that really default formatting?

6. Why are commas not needed in match arms with braces? Further, why are
they discouraged as a matter of style?
It would be more consistent to have commas on all items, like in
vector decls.

All of these combine to make a fairly poor experience. I did still
use it to help me find formatting errors, but it was pretty difficult.

---

Why does

```
data[data.len() - 1].push(...);
```

not compile for borrow errors, but

```
let len = data.len();
data[len - 1].push(...);
```

does?

---

I am aware of one specific mistake I made that I've been too lazy
to fix as of yet. Instead of css:get_data, I should have a way of
getting the cell value at an offset.
